import 'package:flutter/material.dart';
import 'package:matrimony2022/database/databse.dart';
import 'package:matrimony2022/screens/pre_login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

 @override
  void initState() {
    super.initState();
    DatabaseHelper().copyPasteAssetFileToRoot().then((value) => {
      print('Database INitialized Successfully')
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Matrimony',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PreLoginPage(),
    );
  }
}
