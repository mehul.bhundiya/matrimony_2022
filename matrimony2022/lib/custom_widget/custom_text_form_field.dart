import 'package:flutter/material.dart';

class CustomTextFormField extends StatefulWidget {
  String? hintText;
  IconData? prefixIcon = Icons.add;

  CustomTextFormField({required this.hintText, this.prefixIcon});

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (value) {
        if (value!.length == 0) {
          return 'Please enter username';
        } else if (!(RegExp(
                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(value.toString()))) {
          return 'Please enter valid email';
        }
      },
      decoration: InputDecoration(
        prefixIcon:
            widget.prefixIcon != null ? Icon(widget.prefixIcon) : Container(),
        hintText: widget.hintText!.toUpperCase(),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
