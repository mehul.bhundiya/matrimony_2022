import 'package:flutter/material.dart';
import 'package:matrimony2022/screens/user_list.dart';

import '../custom_widget/custom_text_form_field.dart';

class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> formKey = new GlobalKey();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/images/bharat_matrimony_logo.png',
                width: 110,
                height: 70,
              ),
              Text(
                'Matrimony',
                style: TextStyle(
                  fontFamily: 'RobotoBold',
                  fontSize: 25,
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
                      child: Text(
                        'Sign in',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'RobotoBold',
                        ),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 25, 0, 25),
                            child: CustomTextFormField(hintText: 'UserName',),
                          ),
                          CustomTextFormField(hintText: 'Password',),
                        ],
                      ),
                    ),

                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(20, 30, 20, 3),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => UserListScreen()));
                        },
                        child: Text(
                          'Login',
                        ),
                      ),
                    ),
                    Text(
                      'or sign in with:',
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FloatingActionButton(
                          onPressed: () {},
                          child: Icon(Icons.facebook),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        FloatingActionButton(
                          onPressed: () {},
                          child: Icon(Icons.facebook),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        FloatingActionButton(
                          onPressed: () {},
                          child: Icon(Icons.facebook),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(child: Text('Don\'t Have an Account?')),
                  Text('Forgot Password'),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
